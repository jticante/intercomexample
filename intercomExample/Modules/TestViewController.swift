//
//  ViewController.swift
//  intercomExample
//
//  Created by developer on 3/20/20.
//  Copyright © 2020 juncko. All rights reserved.
//

import UIKit
import Intercom

class TestViewController: UIViewController {
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var openIntercomButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    private var loggedIn: Bool! = false {
        didSet {
            loginButton.isEnabled = !loggedIn
            logoutButton.isEnabled = loggedIn
            openIntercomButton.isEnabled = loggedIn
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loggedIn = UserDefaults.standard.string(forKey: Constants.AppPreferences.emailKey) != nil
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let loginAlert = UIAlertController(title: "User Login", message: "Type in an email address", preferredStyle: .alert)
        loginAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        loginAlert.addAction(UIAlertAction(title: "Login", style: .default, handler: { (action: UIAlertAction) in
            self.handleLogin(loginAlert)
        }))
        loginAlert.addTextField { (textField : UITextField) in
            textField.placeholder = "Enter email"
        }
        loginAlert.view.setNeedsLayout()
        present(loginAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func openIntercomAction(_ sender: UIButton) {
         Intercom.presentMessenger()
    }
    
    @IBAction func logOutAction(_ sender: UIButton) {
        Intercom.logout()
        UserDefaults.standard.removeObject(forKey: Constants.AppPreferences.emailKey)
        loggedIn = false
    }
    
    private func setConfigurationUser()  {
        let userAttributes = ICMUserAttributes()
        userAttributes.name = "Bob"
        userAttributes.email = "bob@example.com"
        userAttributes.customAttributes = ["paid_subscriber": true,
        "monthly_spend"  : 155.5,
        "team_mates"     : 3]
        Intercom.updateUser(userAttributes)
    }
    
   private func setConfigurationCompany()  {
        let company = ICMCompany()
        company.name = "My Company"
        company.companyId = "abc1234"
        let userAttributes = ICMUserAttributes()
        userAttributes.companies = [company]
        Intercom.updateUser(userAttributes)
    }
    
    private func registerEvent(){
        Intercom.logEvent(withName: "ordered_item", metaData:[
        "order_date": 1392036272,
        "stripe_invoice":"inv_3434343434",
        "order_number": [
            "value":"3434-3434",
            "url":"https://example.org/orders/3434-3434"]
        ])
    }
    
    func handleLogin(_ alertController: UIAlertController) -> Void {
        
        let textField = (alertController.textFields?.first)!
        if !(textField.text?.isEmpty)! {
            //Start tracking the user with Intercom
            Intercom.registerUser(withEmail: textField.text!)
            
            UserDefaults.standard.set(textField.text!, forKey: Constants.AppPreferences.emailKey)
            loggedIn = true
        }
    }
    
}

