//
//  AppDelegate.swift
//  intercomExample
//
//  Created by developer on 3/20/20.
//  Copyright © 2020 juncko. All rights reserved.
//

import UIKit
import Intercom

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Intercom.setApiKey("ios_sdk-b86adb3223f5ec55b6c8e3e39b7557b815029ae0", forAppId:"je9i3xqa")
        Intercom.setLauncherVisible(true)
        Intercom.enableLogging()
        let defaults = UserDefaults.standard
        if let email = defaults.string(forKey: Constants.AppPreferences.emailKey) {
            Intercom.registerUser(withEmail: email)
        }
        return true
    }
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }


}

