//
//  SceneDelegate.swift
//  intercomExample
//
//  Created by developer on 3/20/20.
//  Copyright © 2020 juncko. All rights reserved.
//

import UIKit
import Intercom

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        Intercom.setApiKey("ios_sdk-b86adb3223f5ec55b6c8e3e39b7557b815029ae0", forAppId:"je9i3xqa")
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }
}

